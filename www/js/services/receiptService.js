app.factory('Receipt', function() {
    var receipt = {};

    return {
    	getReceipt : function () { return receipt; },
    	clearReceipt : function () { receipt = {}; },
    	setReceipt : function (newReceipt) { receipt = angular.copy(newReceipt); },
    	isEmpty : function () {
    		if (angular.equals({}, receipt))
    			return true;
    		
    		return false; 
    	}
    

    }
});