app.controller('ReceiptsCtrl', function($scope, $ionicPopup, Authentication){
	$scope.isReceiptShown = function(receipt) { return $scope.shownReceipt === receipt; };
	$scope.toggleReceipt = function(receipt) {
    	if ($scope.isReceiptShown(receipt)) 
      		$scope.shownReceipt = null;
    	else
      		$scope.shownReceipt = receipt;
  	};


/*  	var tt = Authentication.getUser();
  	debugger;
*/
	$scope.categories = [ 
		"Είδη διατροφής", 
		"Οινοπνευµατώδη ποτά & καπνός", 
		"Είδη ένδυσης & υπόδησης", 
		"Στέγαση", 
		"∆ιαρκή αγαθά",
		"Υγεία", 
		"Μεταφορές", 
		"Επικοινωνίες",
		"Αναψυχή & πολιτισµός", 
		"Εκπαίδευση",
		"Ξενοδοχεία, καφενεία & εστιατόρια",
		"∆ιάφορα αγαθά & υπηρεσίες"
	];

	function applyFilter() {
 		var filteredReceipts = [];
 		for(var i=0; i<$scope.receipts_raw.length; i++) {
 			for(var j=0; j<$scope.receipts_raw[i].items.length; j++) {
 				if (angular.equals($scope.receipts_raw[i].items[j].name, "ΚΑΤΗΓΟΡΙΑ") && angular.equals($scope.receipts_raw[i].items[j].value, $scope.selectedCategory))
 					filteredReceipts.push($scope.receipts_raw[i]);
 			}
 		}

 		$scope.receipts=angular.copy(filteredReceipts);
 	};

	$scope.showCategoriesPopup = function () {
 		$scope.categoriesPopup = $ionicPopup.show({
 			cssClass: 'category-popup',
    		template: '<ion-scroll zooming="false" direction="y" style="height: 150px;"><ul class="list"><li class="item" ng-repeat="category in categories" ng-click="getCategoryFilter($index);modal.hide()">{{category}}</li></ul></ion-scroll>',
    		title: 'Επιλέξτε κατηγορία',
    		scope: $scope,
    		buttons: [ { 
    			text: 'Καθαρισμός' ,
    			onTap: function(e) {debugger;
		          	$scope.clearFilter();
		          	$scope.categoriesPopup.close();
		        }
    		}]
      	});
 	};

 	$scope.getCategoryFilter = function (index) {
 		$scope.selectedCategory = $scope.categories[index];
 		applyFilter();
   		$scope.categoriesPopup.close();
	};

	$scope.clearFilter = function () {
 		$scope.selectedCategory = null;
 		$scope.receipts = angular.copy($scope.receipts_raw);
 	};

	$scope.receipts_raw = [
		{
			name : "ΑΡΤΟΣ & ΤΕΧΝΗ",
			aa : "1231",
			am : "14001468",
			items : [ 
				{ name : "ΔΙΕΥΘΥΝΣΗ", value : "Σεβαστουπώλεως 29" },
				{ name : "ΤΗΛΕΦΩΝΟ", value : "210 1234567" },
				{ name : "ΑΦΜ", value : "139065084" },
				{ name : "ΔΟΥ", value : "Ψυχικού" },
				{ name : "ΗΜΕΡΟΜΗΝΙΑ", value : "21-4-2014 12:22" },
				{ name : "ΚΑΤΗΓΟΡΙΑ", value : "Είδη διατροφής" },
				{ name : "ΦΠΑ", value : "9.75€"	},
				{ name : "ΣΥΝΟΛΟ", value : "12.00€" }
			]
		},
		{
			name : "ΑΡΤΟΣ & ΤΕΧΝΗ",
			aa : "1231",
			am : "14001468",
			items : [ 
				{ name : "ΔΙΕΥΘΥΝΣΗ", value : "Σεβαστουπώλεως 29" },
				{ name : "ΤΗΛΕΦΩΝΟ", value : "210 1234567" },
				{ name : "ΑΦΜ", value : "139065084" },
				{ name : "ΔΟΥ", value : "Ψυχικού" },
				{ name : "ΗΜΕΡΟΜΗΝΙΑ", value : "21-4-2014 12:22" },
				{ name : "ΚΑΤΗΓΟΡΙΑ", value : "Στέγαση" },
				{ name : "ΦΠΑ", value : "9.75€"	},
				{ name : "ΣΥΝΟΛΟ", value : "12.00€" }
			]
		},
		{
			name : "ΑΡΤΟΣ & ΤΕΧΝΗ",
			aa : "1231",
			am : "14001468",
			items : [ 
				{ name : "ΔΙΕΥΘΥΝΣΗ", value : "Σεβαστουπώλεως 29" },
				{ name : "ΤΗΛΕΦΩΝΟ", value : "210 1234567" },
				{ name : "ΑΦΜ", value : "139065084" },
				{ name : "ΔΟΥ", value : "Ψυχικού" },
				{ name : "ΗΜΕΡΟΜΗΝΙΑ", value : "21-4-2014 12:22" },
				{ name : "ΚΑΤΗΓΟΡΙΑ", value : "Στέγαση" },
				{ name : "ΦΠΑ", value : "9.75€"	},
				{ name : "ΣΥΝΟΛΟ", value : "12.00€" }
			]
		}
	]; 

	$scope.receipts=angular.copy($scope.receipts_raw);

	$scope.showAllTabData = function () {
		$scope.receipts = angular.copy($scope.receipts_raw);
	}
	$scope.showMonthTabData = function () {
		$scope.receipts = angular.copy($scope.receipts_raw);
	}
});