app.controller('ProfileCtrl', function($scope, $ionicPopup, Receipt, $rootScope){
	$scope.user = {
		email : "imktks@gmail.com"
	}

	$scope.data = [
      {
        value: 300,
        color:'#F7464A',
        highlight: '#FF5A5E'
      },
      {
        value: 50,
        color: '#46BFBD',
        highlight: '#5AD3D1'
      },
      {
        value: 100,
        color: '#FDB45C',
        highlight: '#FFC870'
      }
    ];

    
    $scope.options =  {		// Chart.js Options
      	responsive: true,
      	segmentShowStroke : true,
      	segmentStrokeColor : '#fff',
      	segmentStrokeWidth : 2,
      	percentageInnerCutout : 80, // This is 0 for Pie charts
      	animationSteps : 100,
      	animationEasing : 'easeInOutQuad',
      	animateRotate : true,
      	animateScale : false,
      	tooltipEvents: [],
		showTooltips: true,
      	legendTemplate : ' ',
      	onAnimationComplete: function(){ 
      		$rootScope.$broadcast('animation_completed');
      	}
    };
});