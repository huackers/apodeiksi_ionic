app.controller('ConfirmCtrl', function($scope, $ionicPopup, Receipt){
	if (Receipt.isEmpty())
		$scope.title = "Προσθήκη απόδειξης";
	else
		$scope.title = "Επιβεβαίωση στοιχείων";
	
	$scope.receipt = {};
	$scope.receipt.date = new Date();

	$scope.addReceipt = function (form) {
		if(!form.$valid) {
			form.sname.$pristine=false;
			form.address.$pristine=false;
			form.afm.$pristine=false;
			form.doy.$pristine=false;
			form.am.$pristine=false;
			form.aa.$pristine=false;
			/*form.date.$pristine=false;*/
			form.category.$pristine=false;
			form.fpa.$pristine=false;
			form.total.$pristine=false;
			debugger;
			return false;
		}
		var tt= $scope.receipt;
		
	}

	$scope.categories = [ 
		"Είδη διατροφής", 
		"Οινοπνευµατώδη ποτά & καπνός", 
		"Είδη ένδυσης & υπόδησης", 
		"Στέγαση", 
		"∆ιαρκή αγαθά",
		"Υγεία", 
		"Μεταφορές", 
		"Επικοινωνίες",
		"Αναψυχή & πολιτισµός", 
		"Εκπαίδευση",
		"Ξενοδοχεία, καφενεία & εστιατόρια",
		"∆ιάφορα αγαθά & υπηρεσίες"
	];

 	$scope.getCategory = function (index) { debugger;
 		$scope.receipt.category = $scope.categories[index];
   		$scope.categoriesPopup.close();
	};

 	$scope.showCategories = function () {
 		$scope.categoriesPopup = $ionicPopup.show({
 			cssClass: 'category-popup',
    		template: '<ion-scroll zooming="false" direction="y" style="height: 150px;"><ul class="list"><li class="item" ng-repeat="category in categories" ng-click="getCategory($index);modal.hide()">{{category}}</li></ul></ion-scroll>',
    		title: 'Επιλέξτε κατηγορία',
    		scope: $scope,
    		buttons: [ { text: 'Ακύρωση' } ]
      	});
 	};
});